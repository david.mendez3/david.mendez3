# Hi, I'm David Méndez

I am a Postdoctoral Research Fellow at the Champalimaud Centre for the Unknown, working on the project '[ALMA: Human Centric Algebraic Machine Learning](https://cordis.europa.eu/project/id/952091)' with the [Mathematics of Behavior and Intelligence Lab](https://polaviejalab.org/) led by Gonzalo de Polavieja. I completed my PhD in 2019 at the University of Malaga, and my advisors were [Cristina Costoya](https://pdi.udc.es/en/File/Pdi/D35WJ) and [Antonio Viruel](https://sites.google.com/view/antonio-viruel/). I also worked as a Turing Research Fellow at the University of Southampton under the supervision of [Rubén Sánchez García](https://www.southampton.ac.uk/people/5x7tz6/doctor-ruben-sanchez-garcia). Some of my research interests are:

- Algebra

- Combinatorics

- Logic

- Machine Learning

- Symbolic Artificial Intelligence

- Topological Data Analysis

- Algebraic Topology and Homotopy Theory

- Model Theory

- Universal Algebra

You can find more information about my research on [my website](https://sites.google.com/view/david-mendez), or in my [resume](resume.pdf).
