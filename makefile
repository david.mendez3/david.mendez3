generate:
	latexmk -lualatex resume.tex
	latexmk -lualatex resume_brief.tex

clean:
	latexmk -c
